# -*- coding: utf-8 -*-

import requests

# This function will pass your text to the machine learning model
# and return the top result with the highest confidence
def classify(text):
    key = "03a72b50-71b4-11e9-a99f-e71f9fea0232c728daf5-cc0a-46d8-a7e6-3b75807c3024"
    url = "https://machinelearningforkids.co.uk/api/scratch/"+ key + "/classify"

    response = requests.get(url, params={ "data" : text

 })

    if response.ok:
        responseData = response.json()
        topMatch = responseData[0]
        return topMatch
    else:
        response.raise_for_status()

def responder_preguntas():


    preg = raw_input ("Adelante, hazme una pregunta")
    respuestas = classify(preg)
    categoria = respuestas["class_name"]
    print(categoria)
    print(respuestas['confidence'])
    if (categoria == "Politica"):
        respuestas = "Lamentablemente aun estoy intentando aprender mas sobre Australia, esto es todo lo que se:\n    -Su presidente se llama Scott Morrinson, quien es primer ministro de Australia desde el 24 de agosto de 2018, y esta al mando del Partido Liberal.\n     Scott es miembro de la Camara de Representantes desde 2007, representando a la division Cooc en Nueva Gales del sur.\n    -Australia es un pais soberano de Oceania\n    -Su forma de gobierno es la monarquia constitucional federal parlamentaria... que nombre tan largo, no?"
    if(categoria == "Relieve"):
        respuestas = "Al este, las llanuras costeras quedan separadas del interior por la Gran Cordillera Divisiria\n   -Al sureste esta compuesto por una serie de cordilleras menores"
    if(categoria == "social"):
        respuestas == "La esperanza de vida de una mujer de Australia es de 84,6 años y de los hombres 80,5 años segun el censo de 2016"
    if(categoria == "economia"):
        respuestas = "Australia está negociando actualmente siete tratados de libre comercio (TLC) con China, Japón, Corea y Malasia, además de tratados de libre comercio regionales o multilaterales con el Consejo de Cooperación del Golfo (CCG), la Asociación Trans-Pacífico y el nuevo acuerdo comercial del Pacífico PACER Plus de comercio exterior."
    if(categoria == "Cultura"):
        respuestas == "Las palabras aborigen e indígena hacen referencia a los pobladores más antiguos de una región. Los aborígenes australianos son los primeros habitantes de Australia y las islas de alrededor. Actualmente, hallamos más de 400 pueblos indígenas australianos con un total de 4.000 habitantes. Cada uno tiene sus propios rasgos culturales y una ubicación geográfica determinada."
    print(respuestas)




while(True):

    print("Hola! Soy Lalo, y me gustaria contarte un par de cosas sobre Australia\n\n")

    responder_preguntas()